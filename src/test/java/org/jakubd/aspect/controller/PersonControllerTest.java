package org.jakubd.aspect.controller;

import org.jakubd.aspect.app.AspectApplication;
import org.jakubd.aspect.model.Person;
import org.jakubd.aspect.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(AspectApplication.class)
@WebAppConfiguration
public class PersonControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private PersonService personService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        reset(personService);
    }

    @Test
    public void processTest() throws Exception {
        mockMvc.perform(get("/person"))
                .andExpect(status().isOk())
                .andExpect(view().name("person"));
    }

    @Test
    public void saveValidTest() throws Exception {

        mockMvc.perform(post("/person")
                .param("save", "true")
                .param("person.login", "asd")
                .param("person.password", "asd"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")))
                .andReturn();
    }

    @Test
    public void saveInvalidTest() throws Exception {

        mockMvc.perform(post("/person")
                .param("save", "true")
                //.param("person.login", "asd")
                .param("person.password", "asd"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("invalid")))
                .andReturn();
    }

    @Test
    public void saveErrorTest() throws Exception {
        doThrow(new IllegalArgumentException()).when(personService).save(any(Person.class));

        mockMvc.perform(post("/person")
                .param("save", "true")
                .param("person.login", "asd")
                .param("person.password", "asd"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("error")))
                .andReturn();
    }

}