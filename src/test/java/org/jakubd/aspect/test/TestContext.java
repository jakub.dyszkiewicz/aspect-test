package org.jakubd.aspect.test;

import org.jakubd.aspect.service.PersonService;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@Configuration
@ComponentScan("org.jakubd.aspect")
public class TestContext {

    @Bean
    public PersonService personService() {
        return Mockito.mock(PersonService.class);
    }
}
