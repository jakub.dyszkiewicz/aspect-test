package org.jakubd.aspect.form;

import lombok.Getter;
import lombok.Setter;
import org.jakubd.aspect.model.Person;

@Getter
@Setter
public class PersonForm {
    private Person person = new Person();
}
