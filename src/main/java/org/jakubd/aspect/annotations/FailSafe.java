package org.jakubd.aspect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Let controller's method fail silently.
 * If action fails, then value of message() will be added at "message" key to model
 *
 * Model is required argument of method annotated with @FailSafe
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FailSafe {
    String message() default "error";
}
