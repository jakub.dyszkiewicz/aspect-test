package org.jakubd.aspect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Executes body of method only if model attributes are valid
 *
 * If attributes are valid, then value of success() will be added at "message" key
 * Otherwise the value of invalid() will be added at "message" key
 *
 * Model and BindingResult are required arguments of method annotated with @OnlyValid
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OnlyValid {
    String success() default "success";
    String error() default "invalid";
}
