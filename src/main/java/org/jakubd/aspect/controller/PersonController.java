package org.jakubd.aspect.controller;

import lombok.extern.slf4j.Slf4j;
import org.jakubd.aspect.annotations.FailSafe;
import org.jakubd.aspect.annotations.OnlyValid;
import org.jakubd.aspect.form.PersonForm;
import org.jakubd.aspect.service.PersonService;
import org.jakubd.aspect.validator.PersonValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/person")
@Slf4j
public class PersonController {
    public static final String VIEW_NAME = "person";
    public static final String FORM = "form";
    public static final String MESSAGE_KEY = "message";

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonValidator validator;

    @RequestMapping(method = RequestMethod.GET)
    public String process(Model model) {
        model.addAttribute(FORM, new PersonForm());
        return VIEW_NAME;
    }

    @FailSafe @OnlyValid
    @RequestMapping(method = RequestMethod.POST, params = "save")
    public String save(Model model, @Valid @ModelAttribute(FORM) PersonForm form, BindingResult result) {
        personService.save(form.getPerson());
        return VIEW_NAME;
    }

    /*
    @RequestMapping(method = RequestMethod.POST, params = "save")
    public String save(Model model, @Valid @ModelAttribute(FORM) PersonForm form, BindingResult result) {
        log.debug("save > START");
        if (!result.hasErrors()) {
            try {
                personService.save(form.getPerson());
                model.addAttribute(MESSAGE_KEY, "success");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                model.addAttribute(MESSAGE_KEY, "error");
            }
        } else {
            model.addAttribute(MESSAGE_KEY, "invalid");
        }
        log.debug("save > END");
        return VIEW_NAME;
    }
    */

    @InitBinder(FORM)
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.addValidators(validator);
    }
}
