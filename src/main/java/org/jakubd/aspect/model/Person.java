package org.jakubd.aspect.model;

import lombok.Data;

@Data
public class Person {
    private String id;
    private String login;
    private String password;
}
