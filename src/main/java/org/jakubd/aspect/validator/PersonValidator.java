package org.jakubd.aspect.validator;

import org.jakubd.aspect.form.PersonForm;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PersonValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(PersonForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PersonForm form = (PersonForm) target;
        validateLogin(form.getPerson().getLogin(), errors);
        validatePassword(form.getPerson().getPassword(), errors);
    }

    private void validateLogin(String login, Errors errors) {
        if (StringUtils.isEmpty(login)) {
            errors.reject("person.login");
        }
    }

    private void validatePassword(String password, Errors errors) {
        if (StringUtils.isEmpty(password)) {
            errors.reject("person.password");
        }
    }
}
