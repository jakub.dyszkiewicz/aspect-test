package org.jakubd.aspect.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages = "org.jakubd.aspect")
public class AspectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AspectApplication.class);
    }
}
