package org.jakubd.aspect.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.jakubd.aspect.annotations.OnlyValid;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Aspect
@Component
@Slf4j
public class OnlyValidAspect extends ControllerAspect {

    @Around("controllerActionMethod() && @annotation(org.jakubd.aspect.annotations.OnlyValid)")
    public Object validate(ProceedingJoinPoint joinPoint) throws Throwable {
        String viewName = (String) AspectUtils.getFieldOfClassUnsafe(joinPoint, VIEW_NAME);
        Model model = AspectUtils.getMethodArgumentUnsafe(joinPoint, Model.class);
        OnlyValid onlyValid = AspectUtils.getAnnotationUnsafe(joinPoint, OnlyValid.class);
        BindingResult bindingResult = AspectUtils.getMethodArgumentUnsafe(joinPoint, BindingResult.class);

        if (bindingResult.hasErrors()) {
            model.addAttribute(MESSAGE_KEY, onlyValid.error());
            return viewName;
        } else {
            Object toReturn = joinPoint.proceed();
            model.addAttribute(MESSAGE_KEY, onlyValid.success());
            return toReturn;
        }
    }
}
