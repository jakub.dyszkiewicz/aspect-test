package org.jakubd.aspect.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
public class LoggingAspect extends ControllerAspect {

    @Around("controllerActionMethod()")
    public Object logAction(ProceedingJoinPoint joinPoint) throws Throwable {
        long executionTime = System.currentTimeMillis();
        String actionName = joinPoint.getSignature().getName();
        log.info(actionName + " > START");
        Object returnValue = joinPoint.proceed();
        executionTime = System.currentTimeMillis() - executionTime;
        log.info(actionName + " > END with {} ms", executionTime);
        return returnValue;
    }
}
