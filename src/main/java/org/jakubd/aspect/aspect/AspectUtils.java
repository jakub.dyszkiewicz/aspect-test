package org.jakubd.aspect.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Optional;

public class AspectUtils {

    @SuppressWarnings("unchecked")
    public static <T> Optional<T> getMethodArgument(JoinPoint joinPoint, Class<T> clazz) {
        return (Optional<T>) Arrays.stream(joinPoint.getArgs())
                .filter(arg -> clazz.isAssignableFrom(arg.getClass()))
                .findAny();
    }

    public static <T> T getMethodArgumentUnsafe(JoinPoint joinPoint, Class<T> clazz) {
        return getMethodArgument(joinPoint, clazz)
                .orElseThrow(() -> new RuntimeException("Method argument of " + clazz.getSimpleName() + " has to be present"));
    }

    public static <T extends Annotation> Optional<T> getAnnotation(JoinPoint joinPoint, Class<T> clazz) {
        return Optional.ofNullable(((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(clazz));
    }

    public static <T extends Annotation> T getAnnotationUnsafe(JoinPoint joinPoint, Class<T> clazz) {
        return getAnnotation(joinPoint, clazz)
                .orElseThrow(() -> new RuntimeException("Annotation " + clazz.getSimpleName() + " has to be present"));
    }

    public static Optional<?> getFieldOfClass(JoinPoint joinPoint, String fieldName) {
        try {
            return Optional.ofNullable(joinPoint.getSignature().getDeclaringType().getDeclaredField(fieldName).get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return Optional.empty();
        }
    }

    public static Object getFieldOfClassUnsafe(JoinPoint joinPoint, String fieldName) {
        return getFieldOfClass(joinPoint, fieldName)
                .orElseThrow(() -> new RuntimeException("Field " + fieldName + " has to present in class " + joinPoint.getSignature().getDeclaringType().getSimpleName()));
    }
}
