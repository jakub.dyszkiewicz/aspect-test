package org.jakubd.aspect.aspect;

import org.aspectj.lang.annotation.Pointcut;

public abstract class ControllerAspect {
    public static final String MESSAGE_KEY = "message";
    public static final String VIEW_NAME = "VIEW_NAME";

    @Pointcut("execution(@org.springframework.web.bind.annotation.RequestMapping * org.jakubd.aspect.controller..*.*(..))")
    void controllerActionMethod() {}
}
