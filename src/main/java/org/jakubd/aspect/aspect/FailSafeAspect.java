package org.jakubd.aspect.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.jakubd.aspect.annotations.FailSafe;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

@Aspect
@Component
@Slf4j
public class FailSafeAspect extends ControllerAspect {

    @Around("controllerActionMethod() && @annotation(org.jakubd.aspect.annotations.FailSafe)")
    public Object failSafe(ProceedingJoinPoint joinPoint) throws Throwable {
        String viewName = (String) AspectUtils.getFieldOfClassUnsafe(joinPoint, VIEW_NAME);
        Model model = AspectUtils.getMethodArgumentUnsafe(joinPoint, Model.class);
        FailSafe annotation = AspectUtils.getAnnotationUnsafe(joinPoint, FailSafe.class);

        try {
            return joinPoint.proceed();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            model.addAttribute(MESSAGE_KEY, annotation.message());
            return viewName;
        }
    }
}
