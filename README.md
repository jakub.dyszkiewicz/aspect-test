Test application that transforms logging, validation and error checking to Aspects, so you can focus on writing actual logic in controller.

Before
```java
@RequestMapping(method = RequestMethod.POST, params = "save")
public String save(Model model, @Valid @ModelAttribute(FORM) PersonForm form, BindingResult result) {
    log.debug("save > START");
    if (!result.hasErrors()) {
        try {
            personService.save(form.getPerson());
            model.addAttribute(MESSAGE_KEY, "success");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            model.addAttribute(MESSAGE_KEY, "error");
        }
    } else {
        model.addAttribute(MESSAGE_KEY, "invalid");
    }
    log.debug("save > END");
    return VIEW_NAME;
}
```

After
```java
@FailSafe @OnlyValid
@RequestMapping(method = RequestMethod.POST, params = "save")
public String save(Model model, @Valid @ModelAttribute(FORM) PersonForm form, BindingResult result) {
    personService.save(form.getPerson());
    return VIEW_NAME;
}
```

Transformation was done using 3 aspects

1. [LoggingAspect](/src/main/java/org/jakubd/aspect/aspect/LoggingAspect.java)

Works on all controller methods. Logs when method starts and ends. Bonus, it's also a benchmark.

2. [FailSafeAspect](/src/main/java/org/jakubd/aspect/aspect/FailSafeAspect.java)

Works on all controller methods annotated with @FailSafe. It catches all exceptions, logs them and adds generic message to model.

*Note:* you can also specify message.

3. [OnlyValidAspect](/src/main/java/org/jakubd/aspect/aspect/OnlyValidAspect.java)

Works on all controller methods annotated with @OnlyValid. It checks whether BindResult contains errors. The method's body will not be executed if there are any and proper message will be added to model. Otherwise, success message will be added.

*Note:* you can also specify messages.